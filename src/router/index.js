import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Biometric from '../views/Biometric.vue'
import Credentials from '../views/Credentials.vue'
import Dashboard from '../views/Dashboard.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/biometric',
    name: 'Biometric',
    component: Biometric
  },
  {
    path: '/credentials',
    name: 'Credentials',
    component: Credentials
  }, 
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
]

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL, routes
})

export default router
